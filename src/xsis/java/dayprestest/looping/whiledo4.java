package xsis.java.dayprestest.looping;

import java.util.Scanner;

public class whiledo4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double sum=0 ,n=0;
        int i=1;
        while (i<=4){
            System.out.println("input number ke:"+i+":");
            n=scanner.nextDouble();
            if(n%2==0){
                sum=sum+n;
            }
            i++;
        }
        System.out.println("total : "+sum);
    }
}
