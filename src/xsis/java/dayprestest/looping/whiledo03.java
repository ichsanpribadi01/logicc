package xsis.java.dayprestest.looping;

import java.util.Scanner;

public class whiledo03 {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int i=1;
        double sum=0,n;
        while (i<=4){
            System.out.println("number ke "+i+":");
            n= scanner.nextDouble();
            sum = sum+n;
            i++;
        }
        System.out.println("total sum :"+sum
        );
    }
}
