package xsis.java.dayprestest.oopMateri.encapsulotion;

public interface EmployeeInterface {
    String showEmployeeInfo();
    double showEmployee();
}
