package xsis.java.dayprestest.oopMateri.encapsulotion;

import java.time.LocalDate;

public class Manager extends Employee implements EmployeeInterface{
    private  double commision;
    private double totalbonus;

    @Override
    public String showEmployeeInfo() {
        //get strimg to string method
        return toString();
    }

    @Override
    public double showEmployee() {
        return 0;
    }

    public double getTotalbonus() {
        return super.getSalary()+
                super.getSalary()*this.commision/100;
    }

    public void setTotalbonus(double totalbonus) {
        this.totalbonus = totalbonus;
    }

    public Manager(int empId, String empName, double salary, LocalDate joindate, double commision) {
        super(empId, empName, salary, joindate);
        this.commision=commision;

    }

    public double getCommision() {
        return commision;
    }

    public void setCommision(double commision) {
        this.commision = commision;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "commision=" + commision +
                ", totalbonus=" + getTotalbonus() +
                "} " + super.toString();
    }
}
