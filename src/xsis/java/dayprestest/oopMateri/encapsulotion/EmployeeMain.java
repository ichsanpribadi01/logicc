package xsis.java.dayprestest.oopMateri.encapsulotion;

import java.time.LocalDate;


public class EmployeeMain {
    public static void main(String[] args) {
        Employee emp = new Employee(123, "jhon", 12500,
                            LocalDate.of(2020 , 1,8));
        Employee emp1 = new Employee(126,"budi",100000);

        Manager mg = new Manager(123,"jhon",15000.00,
                LocalDate.of(2018,12,18),10);

        EmployeeInterface ef = new Manager(123,"jhon",15000.00,
                LocalDate.of(2018,12,18),10);

        System.out.println(ef.showEmployeeInfo());
        System.out.println(mg.toString());
        System.out.println(Employee.total);
        Employee[] employees = {emp,emp1};
        initialArray(employees);
        UpdateSalary(employees,126,200000);

    }

    public static void initialArray(Employee[] employees){
        for (int i = 0; i <employees.length ; i++) {
            System.out.println(employees[i].toString());
        }
        System.out.println("=================================");

    }
    public static void updateJoindate(Employee[] employees){
        for (int i = 0; i <employees.length ; i++) {
            if (employees[i].getJoindate()==null){
                employees[i].setJoindate(LocalDate.of(2020,1,9));
            }
            System.out.println(employees[i].toString());
        }

    }
    public  static  void editNameAndSalary(Employee[] employees){
        for (int i = 0; i <employees.length ; i++) {
            if(employees[i].getEmpName()=="jhon"){
                employees[i].setEmpName("jhon D ace");
                employees[i].setSalary(20000);
            }
            System.out.println(employees[i].toString());
        }

    }
    public  static  void UpdateSalary(Employee[] employees,int empId){
        for (int i = 0; i <employees.length ; i++) {
            if(employees[i].getEmpId()==empId){
                employees[i].setSalary(employees[i].getSalary()+5000);
            }
            System.out.println(employees[i].toString());
        }

    }
    public  static  void UpdateSalary(Employee[] employees,int empId,double salary){
        System.out.println("========update salary by id==============");
        for (int i = 0; i <employees.length ; i++) {
            if(employees[i].getEmpId()==empId){
                employees[i].setSalary(employees[i].getSalary()+salary);
            }
            System.out.println(employees[i].toString());

        }

    }

}
