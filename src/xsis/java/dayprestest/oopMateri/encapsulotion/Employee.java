package xsis.java.dayprestest.oopMateri.encapsulotion;

import java.time.LocalDate;


public class Employee {
    static int total=0;
    private  int empId;
    private String empName;
    private double salary;
    private LocalDate joindate;



    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public LocalDate getJoindate() {
        return joindate;
    }

    public void setJoindate(LocalDate joindate) {
        this.joindate = joindate;
    }

    public Employee(int empId, String empName, double salary) {
        this.empId = empId;
        this.empName = empName;
        this.salary = salary;
     total++;
    }
    public Employee(int empId, String empName, double salary, LocalDate joindate) {
        this.empId = empId;
        this.empName = empName;
        this.salary = salary;
        this.joindate = joindate;
        total++;
    }
    @Override
    public String toString() {
        return "Employee{" +
                "empId=" + empId +
                ", empName='" + empName + '\'' +
                ", salary=" + salary +
                ", joindate=" + joindate +
                '}';
    }
}
