package xsis.java.dayprestest.oopMateri;

import java.time.LocalDate;

public class EmployeeMain {
    public static void main(String[] args) {
        Employee emp = new Employee(123, "jhon", 12500,
                LocalDate.of(2020 , 1,8));

        emp.salary=5000000;
        System.out.println("Employed name :" +emp.empName+" "+emp.salary);
        System.out.println(emp.toString());
    }
}
