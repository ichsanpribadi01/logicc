package xsis.java.dayprestest.oopMateri;

import java.time.LocalDate;

public class Employee {
    int empId;
    String empName;
    double salary;
    LocalDate joindate;

    public Employee(int empId, String empName, double salary, LocalDate joindate) {
        this.empId = empId;
        this.empName = empName;
        this.salary = salary;
        this.joindate = joindate;
    }
}
