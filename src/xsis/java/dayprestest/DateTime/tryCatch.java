package xsis.java.dayprestest.DateTime;

import java.util.InputMismatchException;
import java.util.Scanner;

public class tryCatch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("input a number : ");
        try {
            int number=scanner.nextInt();
            System.out.println("number = "+number);
        }catch (InputMismatchException e){
            System.out.println("error integer number exception");
        }
    }
}
