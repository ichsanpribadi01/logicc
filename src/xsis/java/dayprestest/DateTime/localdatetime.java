package xsis.java.dayprestest.DateTime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class localdatetime {
    public static void main(String[] args) {
        LocalDate startdate = LocalDate.of(2020,01,01);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input EndDate : ");
        int day = scanner.nextInt();
        int month = scanner.nextInt();
        int year = scanner.nextInt();
        LocalDate endtdate = LocalDate.of(year,month,day);

        long yearBetwen = ChronoUnit.YEARS.between(startdate,endtdate);
        System.out.println("year beetwen : "+yearBetwen );

        long monthBetwen = ChronoUnit.MONTHS.between(startdate,endtdate);
        System.out.println("month beetwen : "+monthBetwen );

        long dayBetwen = ChronoUnit.DAYS.between(startdate,endtdate);
        System.out.println("day beetwen : "+dayBetwen );

        LocalTime startTime = LocalTime.of(01,50);
        System.out.println("Input End time");
        int hour = scanner.nextInt();
        int minute = scanner.nextInt();
        LocalTime endTime =LocalTime.of(hour,minute);
        long hourBetwen = ChronoUnit.HOURS.between(startTime,endTime);
        System.out.println("hour beetwen : "+hourBetwen);
    }
}
