package xsis.java.dayprestest.conditions;

import java.util.Scanner;

public class if02 {
    public static void main(String[] args) {
        String sentence, first_char, last_char;
        boolean sentence_is_okay;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter Sentence : ");/**/
        sentence = scan.next();
        first_char = "" + sentence.charAt(0);
        last_char = "" + sentence.charAt(sentence.length() - 1);
        sentence_is_okay = true;

        if (first_char.equals(first_char.toUpperCase()) == false) {
            sentence_is_okay = false;
        }else if (
                last_char.equals(".") == false
                        && last_char.equals("?") == false
                        && last_char.equals("!") == false) {
            sentence_is_okay = false;
        }

        if (sentence_is_okay == true) {
            System.out.println("Sentence is okay!");
        }
    }
}
