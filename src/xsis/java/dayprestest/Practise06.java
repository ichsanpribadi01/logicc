package xsis.java.dayprestest;

import java.util.Scanner;

public class Practise06 {
    static final double pajak = .15;
    public static void main(String[] args) {
        // hitung item produk setelah ditambah pajak
        double itembefortax, itemaftertax;
        Scanner scan = new Scanner(System.in);
        System.out.print("harga barang sebelum pajak  :");
        itembefortax = scan.nextDouble();

        itemaftertax = itembefortax + (itembefortax * pajak);
        System.out.print("harga barng setelah pajak  :" +itemaftertax);


    }
}
