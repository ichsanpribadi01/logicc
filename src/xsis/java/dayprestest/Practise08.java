package xsis.java.dayprestest;

import java.util.Scanner;

public class Practise08 {
    static final double cf = 459.67;
    static final double cd = 1.8;
    public static void main(String[] args) {
        //konversi temperatur dari farenhit ke kelvin
        //rumus : kelvin = (farenhiet + 459.67)/1.8
        double kelvin, faranhit;
        Scanner scan = new Scanner(System.in);
        System.out.print("temperatur in farenhit  :");
        faranhit = scan.nextDouble();
        kelvin = (faranhit + cf)/cd;
        System.out.println("after conver to Kelvin   :"+kelvin);
    }
}
