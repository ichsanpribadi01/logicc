package xsis.java.dayprestest.string;

public class string01 {
    public static void main(String[] args) {
        // get each character from hello variable
        String hello = "Hello Java Developer";
        System.out.println(hello);
        System.out.println("index ke-0 "+hello.charAt(0));
        System.out.println("index ke-3 "+hello.charAt(3));
        System.out.println("index ke-10 "+hello.charAt(10));
    }
}
