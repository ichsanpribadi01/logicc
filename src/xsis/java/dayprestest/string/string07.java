package xsis.java.dayprestest.string;

import java.util.Random;
import java.util.Scanner;

public class string07 {
    public static void main(String[] args) {
        // create username with random number
        String s,result;
        int random, min=100, max=900;
        Random r = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.print("Username : ");
        s = scan.next();
        random = r.nextInt((max-min)+1)+min;
        result = s.substring(0,4)+random;
        System.out.println("Result : "+result);
    }
}
