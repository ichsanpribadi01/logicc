package xsis.java.dayprestest.string;

public class string05 {
    public static void main(String[] args) {
        String s = "Hello Java Programmer";
        String orderName;
        System.out.println("Lenght s : "+s.length());

        // search "Java" index
        System.out.println("index : "+ s.indexOf("Java"));

        // substr untuk get sebuah string dari index ke -i s/d index ke j
        System.out.println("substr : "+s.substring(6,10));
        orderName = s.substring(11,21)+" "+s.substring(0,6)+s.substring(6,10);
        System.out.println("change order name : "+orderName);
    }
}
