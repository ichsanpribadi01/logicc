package xsis.java.dayprestest.string;

public class string04 {
    public static void main(String[] args) {
        // replace word
        String s, st;
        s = "Hello .NET Developer Welcome To Xsis Academy";
        System.out.println(s);
        st = s.replace(".NET","JAVA");
        System.out.println("Replace #1 : "+st);
        // replace Welcome jadi Hello
        st = st.replace("Welcome", "Hello");
        System.out.println("Replace #2 : "+st);
        // replace
        st = s.replace(".NET","JAVA")
                .replace("Welcome","Aloha")
                .replace("Xsis Academy", "XA");
        System.out.println("Replace #3 : "+st);
        // to uppercase to lowercase
        st = s.replace(".NET","JAVA")
                .replace("Welcome","Aloha")
                .replace("Xsis Academy", "XA").toUpperCase();
        System.out.println("Replace #4 : "+st);
    }
}
