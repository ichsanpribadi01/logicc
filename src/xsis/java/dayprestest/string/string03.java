package xsis.java.dayprestest.string;

public class string03 {
    public static void main(String[] args) {
        // replace all whitespace
        String s, st;
        s = "       JAVA       ";
        st = s.trim();
        System.out.println(s);
        System.out.println(st);
        System.out.println(st.toLowerCase());
    }
}
