package xsis.java.dayprestest.math;

public class math01 {
    public static void main(String[] args) {
        // math abs, return number absolute
        int m=-4;
        double d = -4.5;
        System.out.println("Math absolute : "+Math.abs(m));
        System.out.println("Math absolute double: "+Math.abs(d));

        // math pow untuk calculate ekponsensial number
        System.out.println("Math pow : "+Math.pow(2,3));

        // math sqrt
        System.out.println("Math sqrt : "+Math.sqrt(4));

        // math randomize
        System.out.println("Math randomize : "+Math.random());
    }
}
