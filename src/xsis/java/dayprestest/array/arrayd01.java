package xsis.java.dayprestest.array;

public class arrayd01 {
    public static void main(String[] args) {
        String[][] array = new String[2][3];
        array[0][0]="A";
        array[0][1]="B";
        array[0][2]="C";
        array[1][0]="D";
        array[1][1]="E";
        array[1][2]="F";

        for (int i = 0; i <array.length ; i++) {
            for (int j = 0; j <array[i].length ; j++) {
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("=================================");
        for (int i = 0; i < array[0].length-1; i++) {
            for (int j = 0; j <array.length+1 ; j++) {
                System.out.print(array[i][j]+" ");
            }
            System.out.println(" ");
        }
    }
}
