package xsis.java.dayprestest.array;

public class arrayd02 {
    public static void main(String[] args) {
        int[][] array = new int[5][5];
        for (int i = 0; i <array.length ; i++) {
            for (int j = 0; j <array[i].length ; j++) {
                if (i==j){
                    array[i][j]=-1;
                }
                if(i>j){
                    array[i][j]=10;
                }
                if(i<j){
                    array[i][j]=20;
                }
            }
        }
        for (int i = 0; i <array.length ; i++) {
            for (int j = 0; j <array[i].length ; j++) {
                System.out.print(array[i][j]+" ");

            }
            System.out.println();

        }
    }
}
