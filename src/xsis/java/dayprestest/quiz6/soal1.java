package xsis.java.dayprestest.quiz6;

import javafx.scene.transform.Scale;

import java.util.Scanner;

public class soal1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        System.out.println("Jumlah Baris Bintang : ");
        n = scanner.nextInt();
        for (int i = 1; i <=n ; i++) {
            for (int j = 1; j <=i ; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = 1; i <= n ; i++) {
            for (int j = n-1; j >=i ; j--) {
                System.out.print(" ");
            }
            for (int k = 1; k <=i ; k++) {
                System.out.print("*");
            }
            for (int l = 1; l <=i-1 ; l++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
