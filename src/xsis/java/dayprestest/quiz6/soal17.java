package xsis.java.dayprestest.quiz6;


import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class soal17 {
    public static void main(String[] args) {
        Scanner scanner= new Scanner (System.in);
        int tarif_Parkir = 0;

        System.out.println("===========Parkir Java=========");
        System.out.println("1.Motor");
        System.out.println("2.Mobil");
        System.out.print("Kode Kendaraan :");
        int tipe = scanner.nextInt();
        System.out.println("===============================");
        System.out.println("Input waktu Masuk ");
        System.out.print("Tahun : ");
        int tahun = scanner.nextInt();
        System.out.print("Bulan : ");
        int bulan = scanner.nextInt();
        System.out.print("hari : ");
        int hari = scanner.nextInt();
        System.out.print("jam : ");
        int jam = scanner.nextInt();
        System.out.print("Menit : ");
        int menit = scanner.nextInt();
        System.out.print("Detik : ");
        int detik = scanner.nextInt();

        LocalDateTime Jadwalmasuk = LocalDateTime.of(tahun,bulan,hari,jam,menit,detik);
        System.out.println("jadwal masuk : " +Jadwalmasuk);
        System.out.println("===============================");
        System.out.println("Input waktu keluar ");
        System.out.print("Tahun : ");
        int tahunK = scanner.nextInt();
        System.out.print("Bulan : ");
        int bulanK = scanner.nextInt();
        System.out.print("hari : ");
        int hariK = scanner.nextInt();
        System.out.print("jam : ");
        int jamK = scanner.nextInt();
        System.out.print("Menit : ");
        int menitK = scanner.nextInt();
        System.out.print("Detik : ");
        int detikK = scanner.nextInt();

        LocalDateTime Jadwalkeluar = LocalDateTime.of(tahunK,bulanK,hariK,jamK,menitK,detikK);
        System.out.println("jadwal masuk : " +Jadwalkeluar);
        
        long selisihJam = ChronoUnit.HOURS.between(Jadwalmasuk,Jadwalkeluar);
        long selisihmenit = ChronoUnit.MINUTES.between(Jadwalmasuk,Jadwalkeluar);
        int sjam = (int) (selisihJam*60);
        int smenit = (int) (selisihmenit-sjam);
        if(smenit>=30){
            selisihJam=selisihJam+1;
        }
    switch (tipe){
            case 1 :
                if(selisihJam<=1){
                    tarif_Parkir=4000;
                }else if(selisihJam>=1){
                    tarif_Parkir= (int) (4000 + (selisihJam-1)*2000);
                }else if(selisihJam>24){
                    tarif_Parkir= (int) (150000 + 4000+(selisihJam-1)*2000);
                }
                break;
            case 2 :
                if(selisihJam<=1){
                    tarif_Parkir=7000;
                }else if(selisihJam>=1){
                    tarif_Parkir= (int) (7000 + (selisihJam-1)*4000);
                }else if(selisihJam>24){
                    tarif_Parkir= (int) (150000 + 7000+(selisihJam-1)*4000);
                }
                break;

            default:
                tarif_Parkir = 0;
                break;
        }
        System.out.println("===============================");
        System.out.println("==========Biaya Parkir=========");
        System.out.println("===============================");
        if (tipe == 1){
            System.out.println("Jenis Kendaraan : motor");
        }else if (tipe == 2) {
            System.out.println("Jenis Kendaraan : Mobil");
        }else {
            System.out.println("Jenis Kendaraan Tidak Terdaftar");
        }

        System.out.println("Jam Masuk : " + Jadwalmasuk+ " WIB");
        System.out.println("Jam Keluar :" + Jadwalkeluar+ " WIB");
        System.out.println("Lama Parkir :"  + selisihJam + " Jam");
        System.out.println("Tarif Parkir : RP. " + tarif_Parkir);

    }
}
