package xsis.java.dayprestest.quiz6;



public class soal3 {
    public static void main(String[] args) {

            String s = "Aku Sayang Kamu ";
            // cara cepet gunakan replace
            //System.out.println(s.replace("Aku","A*K ").replace("Sayang","S****g ")
            //.replace("Kamu","K**u"));

            // cara sulit
            // deklarasi variable
            int start = 0, end = 0;
            String finalWord = "";
            for (int i = 0; i < s.length(); i++) {
                // ubah tiap kalimat menjadi karakter, dimulai dari index ke-0
                char ch = s.charAt(i);
                end = i;

                // cek apakah ketemu karakter space, jika true maka process
                if (ch == ' ') {
                    // potong kalimat per kata, dengan memotong kata dari index start to end
                    String cutWord = s.substring(start, end);
                    // simpan panjang kata yg sudah dipotong, untuk dilooop
                    int lengtWord = s.substring(start, end).length();
                    // create variable kata untuk simpan per karakter, Aku menjadi A,k,u

                    String kata = "";
                    for (int j = 0; j < lengtWord; j++) {
                        char chin = cutWord.charAt(j);
                        if ((j == 0) || j == lengtWord - 1) {
                            kata = kata + chin;
                        } else if (j > 0 && j < lengtWord - 1) {
                            kata = kata + "*";
                        }
                    }
                    start = end + 1;
                    finalWord = finalWord + " " + kata;
                }
            }
            System.out.println("Final Word : " + finalWord);

        }
    }

