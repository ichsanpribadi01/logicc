package xsis.java.dayprestest.quiz6;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class soal18 {  static final double biayaSewa = 5000.00;
    static final double denda = 550.00;
    static final double discount = .1;
    static final double cashback = .05;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double payment=0,tagihan=0, totalDenda=0, totalDiscount = 0, totalCashback=0;
        int dendaHari;
        boolean isMember = false;
        String member;
        System.out.println("Program Sewa Buku");
        System.out.println("-----------------------");

        System.out.print("Are you member ? : ");
        member = scan.next();
        // ubah input member ke uppercase lalu bandingkan dengan Y
        if (member.toUpperCase().equals("Y")) {
            isMember = true;
        }
        // input start date awal sewa
        System.out.print("Start Date (dd-mm-yyyy): ");
        int startDay = scan.nextInt();
        int startMonth = scan.nextInt();
        int startYear = scan.nextInt();

        System.out.print("End Date (dd-mm-yyyy): ");
        int endDay = scan.nextInt();
        int endMonth = scan.nextInt();
        int endYear = scan.nextInt();

        LocalDate startDate = LocalDate.of(startYear, startMonth, startDay);
        LocalDate endDate = LocalDate.of(endYear, endMonth, endDay);

        long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);

        System.out.print("Input Jumlah buku : ");
        int totalBook = scan.nextInt();
        System.out.println("Total hari sewa: "+daysBetween);

        if (daysBetween > 3) {
            dendaHari = (int) daysBetween - 3;
            totalDenda = denda * dendaHari;
            tagihan = biayaSewa*daysBetween;
            payment = (biayaSewa*daysBetween)+totalDenda;
            if (isMember) {
                totalDiscount = biayaSewa * discount;
                payment = payment-totalDiscount;
            }
        } else if (daysBetween <= 3) {
            tagihan = biayaSewa * daysBetween;
            if (totalBook > 5) {
                if (isMember){
                    totalDiscount = tagihan * discount;
                }
                payment = tagihan-totalDiscount;
                totalCashback = cashback * payment;
            }else{
                if (isMember){
                    totalDiscount = tagihan * discount;
                }
                totalCashback = cashback * payment;
                payment = tagihan-totalDiscount;
            }
        }
        System.out.println("Total denda: "+totalDenda);
        System.out.println("Total discount: "+totalDiscount);
        System.out.println("Total tagihan: "+tagihan);
        System.out.println("Total Payment Akhir: "+payment);
        System.out.println("Total cashback : "+totalCashback);
    }

}
