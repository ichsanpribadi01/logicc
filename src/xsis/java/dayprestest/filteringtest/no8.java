package xsis.java.dayprestest.filteringtest;

import java.util.Scanner;

public class no8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        System.out.println("input panjang array : ");
        n =scanner.nextInt();
        int[] ganjil = new int[n];
        int[] genap = new int[n];
        int[] fibo = new int[n];
        ganjil[0]=1;
        fibo[0]=1;
        for (int i = 1; i <n ; i++) {
            ganjil[i] =ganjil[i-1] +2;
            genap[i]=genap[i-1]+2;
            fibo[i]=(ganjil[i-1] +2)+(genap[i-1]+2);
        }
        for (int i = 0; i <n ; i++) {
            System.out.print(ganjil[i]+" ");
        }
        System.out.println();
        for (int i = 0; i <n ; i++) {
            System.out.print(genap[i]+" ");
        }
        System.out.println();
        for (int i = 0; i <n ; i++) {
            System.out.print(fibo[i]+" ");
        }
    }
}
