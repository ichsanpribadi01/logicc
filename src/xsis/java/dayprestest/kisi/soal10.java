package xsis.java.dayprestest.kisi;

import java.util.Scanner;

public class soal10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        System.out.println("input panjang array : ");
        n =scanner.nextInt();
        long[] ganjil = new long[n];
        ganjil[0]=1;
        long[] genap = new long[n];
        genap[0]=0;
        long[] fibo = new long[n];
        fibo[0]=1;
        for (int i = 1; i <n ; i++) {
            ganjil[i] =ganjil[i-1] +2;
            genap[i]=genap[i-1]+2;
            fibo[i]=(ganjil[i-1] +2)+(genap[i-1]+2);
        }
        for (int i = 0; i <n ; i++) {
            System.out.print(ganjil[i]+" ");
        }
        System.out.println();
        for (int i = 0; i <n ; i++) {
            System.out.print(genap[i]+" ");
        }
        System.out.println();
        for (int i = 0; i <n ; i++) {
            System.out.print(fibo[i]+" ");
        }
    }
}
